<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230530103311 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add Git info to deploy_event';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql(
            <<<SQL
ALTER TABLE
    deploy_event
ADD git_commit_branch VARCHAR(255) DEFAULT NULL,
ADD git_commit_hash VARCHAR(255) DEFAULT NULL,
ADD git_commit_message LONGTEXT DEFAULT NULL
SQL
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql(
            <<<SQL
ALTER TABLE
    deploy_event
DROP git_commit_branch,
DROP git_commit_hash,
DROP git_commit_message
SQL
        );
    }
}
