# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- NLDOC-861: Unit and/or integration tests
- NLDOC-862: CHANGELOG.md

### Changed

- NLDOC-555: migrate without interaction
- NLDOC-862: Splitted off architectures `amd64` and `arm64` to different jobs in
  CI.
