#!/bin/bash
set -e

#function to print bold $1
bold() {
    echo -e "\033[1m$1\033[0m"
}

heading() {
    echo ""
    bold "==$(printf "%${#1}s" | tr " " "=")=="
    bold "  $1  "
    bold "==$(printf "%${#1}s" | tr " " "=")=="
    echo ""
}

cd /var/www/app

heading "Migrating database"

./bin/console doctrine:migrations:migrate --no-interaction

heading "Publishing own version"

bin/console app:publish-deploy-event \
    --app-name="$BUILD_INFO_APP_NAME" \
    --app-version="$BUILD_INFO_APP_VERSION" \
    --environment=production \
    --built-at="$BUILD_INFO_DATE" \
    --deployed-at="$(date -u +"%Y-%m-%dT%H:%M:%SZ")" \
    --git-branch="$BUILD_INFO_GIT_BRANCH" \
    --git-commit-hash="$BUILD_INFO_GIT_COMMIT_HASH" \
    --git-commit-message="$BUILD_INFO_GIT_COMMIT_MESSAGE"

heading "Starting php-fpm"

exec php-fpm
