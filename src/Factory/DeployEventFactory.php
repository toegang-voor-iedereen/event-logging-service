<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\DeployEvent;
use DateTimeImmutable;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<DeployEvent>
 *
 * @method        DeployEvent|Proxy                     create(array|callable $attributes = [])
 * @method static DeployEvent|Proxy                     createOne(array $attributes = [])
 * @method static DeployEvent|Proxy                     find(object|array|mixed $criteria)
 * @method static DeployEvent|Proxy                     findOrCreate(array $attributes)
 * @method static DeployEvent|Proxy                     first(string $sortedField = 'id')
 * @method static DeployEvent|Proxy                     last(string $sortedField = 'id')
 * @method static DeployEvent|Proxy                     random(array $attributes = [])
 * @method static DeployEvent|Proxy                     randomOrCreate(array $attributes = [])
 * @method static RepositoryProxy                       repository()
 * @method static DeployEvent[]|Proxy[]                 all()
 * @method static DeployEvent[]|Proxy[]                 createMany(int $number, array|callable $attributes = [])
 * @method static DeployEvent[]|Proxy[]                 createSequence(iterable|callable $sequence)
 * @method static DeployEvent[]|Proxy[]                 findBy(array $attributes)
 * @method static DeployEvent[]|Proxy[]                 randomRange(int $min, int $max, array $attributes = [])
 * @method static DeployEvent[]|Proxy[]                 randomSet(int $number, array $attributes = [])
 *
 * @phpstan-method        Proxy<DeployEvent> create(array|callable $attributes = [])
 * @phpstan-method static Proxy<DeployEvent> createOne(array $attributes = [])
 * @phpstan-method static Proxy<DeployEvent> find(object|array|mixed $criteria)
 * @phpstan-method static Proxy<DeployEvent> findOrCreate(array $attributes)
 * @phpstan-method static Proxy<DeployEvent> first(string $sortedField = 'id')
 * @phpstan-method static Proxy<DeployEvent> last(string $sortedField = 'id')
 * @phpstan-method static Proxy<DeployEvent> random(array $attributes = [])
 * @phpstan-method static Proxy<DeployEvent> randomOrCreate(array $attributes = [])
 * @phpstan-method static RepositoryProxy<DeployEvent> repository()
 * @phpstan-method static list<Proxy<DeployEvent>> all()
 * @phpstan-method static list<Proxy<DeployEvent>> createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static list<Proxy<DeployEvent>> createSequence(iterable|callable $sequence)
 * @phpstan-method static list<Proxy<DeployEvent>> findBy(array $attributes)
 * @phpstan-method static list<Proxy<DeployEvent>> randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method static list<Proxy<DeployEvent>> randomSet(int $number, array $attributes = [])
 */
final class DeployEventFactory extends ModelFactory
{
    protected static function getClass(): string
    {
        return DeployEvent::class;
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     */
    public function getDefaults(): array
    {
        // Because the order is (1) Build (2) Deploy (3) Event
        $eventSubmittedAt = $this->faker()->dateTime();
        $deployedAt = $this->faker()->dateTime(max: $eventSubmittedAt);
        $builtAt = $this->faker()->dateTime(max: $deployedAt);

        return [
            'uuid' => $this->faker()->uuid(),
            'name' => $this->faker()->text(255),
            'environment' => $this->faker()->text(255),
            'version' => $this->faker()->semver(),
            'builtAt' => DateTimeImmutable::createFromMutable($builtAt),
            'deployedAt' => DateTimeImmutable::createFromMutable($deployedAt),
            'eventSubmittedAt' => DateTimeImmutable::createFromMutable($eventSubmittedAt),
            'gitCommitBranch' => $this->faker()->text(255),
            'gitCommitHash' => $this->faker()->sha1(),
            'gitCommitMessage' => $this->faker()->text(255),
        ];
    }
}
