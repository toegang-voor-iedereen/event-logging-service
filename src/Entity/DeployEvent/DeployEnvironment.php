<?php

declare(strict_types=1);

namespace App\Entity\DeployEvent;

enum DeployEnvironment: string
{
    case Production = 'production';
    case Staging = 'staging';
    case QA = 'qa';
    case Continuous = 'continuous';
    case Sandbox = 'sandbox';

    /**
     * @const string[] Values
     *
     * @var string[]
     */
    public const VALUES = [
        self::Production->value,
        self::Staging->value,
        self::QA->value,
        self::Continuous->value,
        self::Sandbox->value
    ];
}
