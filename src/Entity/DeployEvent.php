<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use App\Entity\DeployEvent\DeployEnvironment;
use App\Filter\LastFilter;
use DateTimeImmutable;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation\Timestampable;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entity for deployment events
 */
#[ORM\Entity]
#[ApiResource(
    description: 'Event triggered when an application in our stack is being deployed.',
    operations: [
        new GetCollection(),
        new Get(),
        new Post(),
    ]
)]

#[ApiFilter(SearchFilter::class, properties: [
    'environment' => 'exact',
])]

#[ApiFilter(
    LastFilter::class,
    properties: [
        "orderByField" => "eventSubmittedAt",
        "orderByDirection" => "DESC",
        "groupBy" => ["name", "environment"]
    ]
)]
class DeployEvent
{
    #[Assert\Uuid]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[ORM\Column(type: Types::GUID, unique: true, nullable: false)]
    #[ApiProperty(identifier: true)]
    private ?string $uuid = null;

    #[Assert\NotNull]
    #[Assert\NotBlank]
    #[ORM\Column(length: 255)]
    #[ApiProperty(description: 'Name of application', example: 'element-structure-converter')]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Assert\Choice(choices: DeployEnvironment::VALUES)]
    #[ApiProperty(
        description: 'Environment application has been deployed in.',
        example: DeployEnvironment::Sandbox->value
    )]
    private ?string $environment = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[ApiProperty(description: 'Version of deployed application.', example: '1.2.3')]
    private ?string $version = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[ApiProperty(
        description: 'Git branch of the specific commit the build was based on.',
        example: 'master'
    )]
    private ?string $gitCommitBranch = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[ApiProperty(
        description: 'Git commit hash of the specific commit the build was based on.',
        example: '90c83d8d0efc9e783def5cfb3c036ac2fa31ce22'
    )]
    private ?string $gitCommitHash = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[ApiProperty(
        description: 'Git commit message of the specific commit the build was based on.',
        example: 'NLDOC-555 add git commit message to build info'
    )]
    private ?string $gitCommitMessage = null;

    #[ORM\Column(type: 'datetimetz_immutable', nullable: true)]
    #[ApiProperty(description: 'When the application or Docker container has been build. '
        . 'Information might not be available.')]
    private ?DateTimeImmutable $builtAt = null;

    #[ORM\Column(type: 'datetimetz_immutable', nullable: true)]
    #[ApiProperty(description: 'When the event has been deployed. Information might not be available.')]
    private ?DateTimeImmutable $deployedAt = null;

    #[ApiProperty(
        description: 'When the event has been submitted to the EventLoggingService',
        writable: false
    )]
    #[Timestampable(on: 'create')]
    #[ORM\Column(type: 'datetimetz_immutable', nullable: false)]
    private DateTimeImmutable $eventSubmittedAt;

    /**
     * @return string|null
     */
    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     * @return $this
     */
    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEnvironment(): ?string
    {
        return $this->environment;
    }

    /**
     * @param string $environment
     * @return $this
     */
    public function setEnvironment(string $environment): self
    {
        $this->environment = $environment;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getVersion(): ?string
    {
        return $this->version;
    }

    /**
     * @param string|null $version
     * @return $this
     */
    public function setVersion(?string $version): self
    {
        $this->version = $version;

        return $this;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getBuiltAt(): ?DateTimeImmutable
    {
        return $this->builtAt;
    }

    /**
     * @param DateTimeImmutable|null $builtAt
     * @return $this
     */
    public function setBuiltAt(?DateTimeImmutable $builtAt): self
    {
        $this->builtAt = $builtAt;

        return $this;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getDeployedAt(): ?DateTimeImmutable
    {
        return $this->deployedAt;
    }

    /**
     * @param DateTimeImmutable|null $deployedAt
     * @return $this
     */
    public function setDeployedAt(?DateTimeImmutable $deployedAt): self
    {
        $this->deployedAt = $deployedAt;

        return $this;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getEventSubmittedAt(): DateTimeImmutable
    {
        return $this->eventSubmittedAt;
    }

    /**
     * @param DateTimeImmutable $pointInTime
     * @return $this
     */
    public function setEventSubmittedAt(DateTimeImmutable $pointInTime): self
    {
        $this->eventSubmittedAt = $pointInTime;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getGitCommitBranch(): ?string
    {
        return $this->gitCommitBranch;
    }

    /**
     * @param string|null $gitCommitBranch
     * @return $this
     */
    public function setGitCommitBranch(?string $gitCommitBranch): self
    {
        $this->gitCommitBranch = $gitCommitBranch;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getGitCommitHash(): ?string
    {
        return $this->gitCommitHash;
    }

    /**
     * @param string|null $gitCommitHash
     * @return $this
     */
    public function setGitCommitHash(?string $gitCommitHash): self
    {
        $this->gitCommitHash = $gitCommitHash;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getGitCommitMessage(): ?string
    {
        return $this->gitCommitMessage;
    }

    /**
     * @param string|null $gitCommitMessage
     * @return $this
     */
    public function setGitCommitMessage(?string $gitCommitMessage): self
    {
        $this->gitCommitMessage = $gitCommitMessage;

        return $this;
    }
}
