<?php

declare(strict_types=1);

namespace App\Filter\LastFilter;

use App\Validator\Constraint\ArrayOnlyHasStringValues;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Properties for LastFilter
 */
class LastFilterProperties
{
    /**
     * @var string $orderByDirection
     *
     * Direction to order by, either ASC or DESC.
     */
    #[Assert\Choice(choices: ['ASC', 'DESC'])]
    protected string $orderByDirection;

    /**
     * @var string[] $groupBy
     *
     * Field names as values, keys are being ignored.
     */
    #[Assert\Type('array')]
    #[Assert\Count(min: 1)]
    #[ArrayOnlyHasStringValues]
    protected array $groupBy;

    /**
     * @var string $orderByField
     *
     * Field to order by
     */
    #[Assert\Type('string')]
    #[Assert\NotBlank]
    protected string $orderByField;

    /**
     * @return string
     */
    public function getOrderByField(): string
    {
        return $this->orderByField;
    }

    /**
     * @param string $orderByField
     * @return LastFilterProperties
     */
    public function setOrderByField(string $orderByField): self
    {
        $this->orderByField = $orderByField;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrderByDirection(): string
    {
        return $this->orderByDirection;
    }

    /**
     * @param string $orderByDirection
     * @return LastFilterProperties
     */
    public function setOrderByDirection(string $orderByDirection): self
    {
        $this->orderByDirection = $orderByDirection;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getGroupBy(): array
    {
        return $this->groupBy;
    }

    /**
     * @param string[] $groupBy
     * @return LastFilterProperties
     */
    public function setGroupBy(array $groupBy): self
    {
        $this->groupBy = $groupBy;

        return $this;
    }
}
