<?php

declare(strict_types=1);

namespace App\Filter;

use ApiPlatform\Doctrine\Orm\Filter\FilterInterface;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use App\Filter\LastFilter\LastFilterProperties;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use InvalidArgumentException;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\Type;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Filter to get last items based on GROUP BY, INNER JOIN and ORDER BY
 */
final class LastFilter extends AbstractFilter implements FilterInterface
{
    protected LastFilterProperties $properties;

    /**
     * @param NormalizerInterface $normalizer
     * @param ValidatorInterface $validator
     * @param EntityManagerInterface $entityManager
     * @param array<string, string|string[]>|null $properties
     *
     * @throws ExceptionInterface
     */
    public function __construct(
        protected NormalizerInterface $normalizer,
        protected ValidatorInterface $validator,
        protected EntityManagerInterface $entityManager,
        ?array $properties = null
    ) {
        if (empty($properties)) {
            throw new InvalidArgumentException(
                sprintf(
                    "Expected some properties passed to %s, but got none.",
                    self::class
                )
            );
        }

        $propertiesNormalizer = new ObjectNormalizer(
            classMetadataFactory: null,
            nameConverter: null,
            propertyAccessor: null,
            propertyTypeExtractor: new ReflectionExtractor()
        );

        $lastFilterProperties = $propertiesNormalizer->denormalize(
            $properties,
            LastFilterProperties::class
        );

        $violations = $this->validator->validate($lastFilterProperties);

        $this->properties = $lastFilterProperties;

        if ($violations->count() > 0) {
            $messages = [];

            foreach ($violations as $violation) {
                $messages[] = sprintf(
                    "%s: %s",
                    $violation->getPropertyPath(),
                    $violation->getMessage()
                );
            }

            throw new InvalidArgumentException(
                sprintf(
                    "Invalid properties passed to %s:\n\n%s",
                    self::class,
                    join("\n", $messages)
                )
            );
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param array<string, mixed> $context
     * @param class-string<object> $resourceClass
     */
    public function apply(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        Operation $operation = null,
        array $context = []
    ): void {
        if (!$this->contextContainsFilter($context)) {
            return;
        }

        $queryFirstBuilder = $this
            ->entityManager
            ->getRepository($resourceClass)
            ->createQueryBuilder('resource')
            ->select(
                [
                    ...array_map(
                        fn(string $field): string => "resource.{$field}",
                        $this->properties->getGroupBy()
                    ),
                    sprintf(
                        "%s(resource.%s) %s",
                        $this->properties->getOrderByDirection() === 'ASC' ? 'MIN' : 'MAX',
                        $this->properties->getOrderByField(),
                        $this->properties->getOrderByField()
                    )
                ]
            );

        foreach ($this->properties->getGroupBy() as $field) {
            $queryFirstBuilder->addGroupBy("resource.{$field}");
        }

        $results = $queryFirstBuilder->getQuery()->getScalarResult();

        if (empty($results) || !is_array($results)) {
            $queryBuilder->andWhere("1 = 0");
            return;
        }

        $alias = $queryBuilder->getRootAliases()[0];
        $orExpression = $queryBuilder->expr()->orX();

        foreach ($results as $index => $result) {
            $innerAndExpression = $queryBuilder->expr()->andX();

            foreach ($result as $field => $fieldValue) {
                $paramName = $field . $index;
                $innerAndExpression->add($queryBuilder->expr()->eq("{$alias}.$field", ":$paramName"));
                $queryBuilder->setParameter($paramName, $fieldValue);
            }

            $orExpression->add($innerAndExpression);
        }

        $queryBuilder->andWhere($orExpression);
    }

    /**
     * @param array<string, mixed> $context
     * @return bool
     */
    protected function contextContainsFilter(array $context): bool
    {
        return !empty($context) &&
            key_exists('filters', $context) &&
            is_array($context['filters']) &&
            key_exists('lasts', $context['filters']) &&
            !empty($context['filters']['lasts']) &&
            $context['filters']['lasts'] === "true";
    }

    /**
     * {@inheritDoc}
     *
     * @return array<string, mixed>
     *
     * @throws InvalidArgumentException
     */
    public function getDescription(string $resourceClass): array
    {
        return [
            "lasts" => [
                "property" => "lasts",
                "type" => Type::BUILTIN_TYPE_BOOL,
                "required" => false,
                "description" => "Get last entities",
                "openapi" => [
                    "example" => true,
                    "allowReserved" => false,
                    "allowEmptyValue" => false,
                    "explode" => false
                ]
            ]
        ];
    }
}
