<?php

declare(strict_types=1);

namespace App\Filter;

use ApiPlatform\Doctrine\Orm\Filter\FilterInterface;

/**
 * Abstract Filter to use in other custom filters.
 */
abstract class AbstractFilter implements FilterInterface
{
}
