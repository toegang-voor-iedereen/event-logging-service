<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\DeployEvent;
use App\Entity\DeployEvent\DeployEnvironment;
use App\Validator\Constraint\IsISO8601;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;

#[AsCommand(
    name: 'app:publish-deploy-event',
    description: 'Create DeployEvent',
)]
class PublishDeployEventCommand extends Command
{
    private const OPTION_APP_NAME = 'app-name';
    private const OPTION_ENVIRONMENT = 'environment';
    private const OPTION_VERSION = 'app-version';
    private const OPTION_BUILT_AT = 'built-at';
    private const OPTION_DEPLOYED_AT = 'deployed-at';
    private const OPTION_GIT_BRANCH = 'git-branch';
    private const OPTION_GIT_COMMIT_HASH = 'git-commit-hash';
    private const OPTION_GIT_COMMIT_MESSAGE = 'git-commit-message';

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(protected EntityManagerInterface $entityManager)
    {
        parent::__construct();
    }

    /**
     * {@inheritDoc}
     */
    protected function configure(): void
    {
        $this
            ->addOption(
                self::OPTION_APP_NAME,
                null,
                InputOption::VALUE_OPTIONAL
            )
            ->addOption(
                self::OPTION_ENVIRONMENT,
                null,
                InputOption::VALUE_OPTIONAL,
                sprintf(
                    "Choose from: %s",
                    join(
                        ', ',
                        array_map(
                            fn(string $choice) => sprintf('"%s"', $choice),
                            DeployEnvironment::VALUES
                        )
                    )
                )
            )
            ->addOption(
                self::OPTION_VERSION,
                null,
                InputOption::VALUE_OPTIONAL
            )
            ->addOption(
                self::OPTION_BUILT_AT,
                null,
                InputOption::VALUE_OPTIONAL
            )
            ->addOption(
                self::OPTION_DEPLOYED_AT,
                null,
                InputOption::VALUE_OPTIONAL
            )
            ->addOption(
                self::OPTION_GIT_BRANCH,
                null,
                InputOption::VALUE_OPTIONAL
            )
            ->addOption(
                self::OPTION_GIT_COMMIT_HASH,
                null,
                InputOption::VALUE_OPTIONAL
            )
            ->addOption(
                self::OPTION_GIT_COMMIT_MESSAGE,
                null,
                InputOption::VALUE_OPTIONAL
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $inputOutput = new SymfonyStyle($input, $output);

        if (!$this->validateInputOptions($input, $output)) {
            return Command::FAILURE;
        }

        $deployEvent = $this->createDeployEvent($input);

        $this->renderDeployEvent($inputOutput, $deployEvent);

        $this->entityManager->persist($deployEvent);
        $this->entityManager->flush();

        $inputOutput->success('Persisted.');

        return Command::SUCCESS;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return bool
     */
    protected function validateInputOptions(InputInterface $input, OutputInterface $output): bool
    {
        $inputOutput = new SymfonyStyle($input, $output);

        $assertions = [
            self::OPTION_APP_NAME => new Assert\NotBlank(),
            self::OPTION_ENVIRONMENT => [
                new Assert\NotBlank(),
                new Assert\Choice(choices: DeployEnvironment::VALUES)
            ],
            self::OPTION_VERSION => new Assert\NotBlank(),
            self::OPTION_BUILT_AT => [new Assert\NotBlank(), new IsISO8601()],
            self::OPTION_DEPLOYED_AT => [new Assert\NotBlank(), new IsISO8601()],
            self::OPTION_GIT_BRANCH => new Assert\NotBlank(),
            self::OPTION_GIT_COMMIT_HASH => new Assert\NotBlank(),
            self::OPTION_GIT_COMMIT_MESSAGE => new Assert\NotBlank(),
        ];

        $values = array_reduce(
            array_keys($assertions),
            function (array $values, string $key) use ($input): array {
                $values[$key] = $this->getOption($input, $key);

                return $values;
            },
            []
        );

        $violations = Validation::createValidator()
            ->validate($values, new Assert\Collection($assertions));

        if (0 === $violations->count()) {
            return true;
        }

        foreach ($violations as $violation) {
            $inputOutput->getErrorStyle()->error(
                sprintf(
                    "Error on option %s: %s",
                    $violation->getPropertyPath(),
                    $violation->getMessage()
                )
            );
        }

        return false;
    }

    /**
     * @param InputInterface $input
     * @param string $optionName
     * @return string|null
     */
    protected function getOption(InputInterface $input, string $optionName): ?string
    {
        $inputValue = $input->getOption($optionName);

        if (!is_string($inputValue) || empty($inputValue)) {
            return null;
        }

        return $this->processEmptyString($inputValue);
    }

    /**
     * @param string $value
     * @return string|null
     */
    protected function processEmptyString(string $value): ?string
    {
        $value = trim($value);

        if (empty($value)) {
            return null;
        }

        return $value;
    }

    /**
     * @param InputInterface $input
     * @return DeployEvent
     */
    protected function createDeployEvent(InputInterface $input): DeployEvent
    {
        $deployEvent = new DeployEvent();

        $deployEvent->setDeployedAt(new DateTimeImmutable('now'));
        $deployEvent->setVersion($this->getOption($input, self::OPTION_VERSION));
        $deployEvent->setGitCommitBranch($this->getOption($input, self::OPTION_GIT_BRANCH));
        $deployEvent->setGitCommitHash($this->getOption($input, self::OPTION_GIT_COMMIT_HASH));
        $deployEvent->setGitCommitMessage($this->getOption($input, self::OPTION_GIT_COMMIT_MESSAGE));

        $appName = $this->getOption($input, self::OPTION_APP_NAME);

        if ($appName) {
            $deployEvent->setName($appName);
        }

        $environment = $this->getOption($input, self::OPTION_ENVIRONMENT);

        if ($environment) {
            $deployEvent->setEnvironment($environment);
        }

        $builtAt = $this->getOption($input, self::OPTION_BUILT_AT);

        if ($builtAt) {
            $deployEvent->setBuiltAt($this->parseDateTime($builtAt));
        }

        $deployedAt = $this->getOption($input, self::OPTION_DEPLOYED_AT);

        if ($deployedAt) {
            $deployEvent->setDeployedAt($this->parseDateTime($deployedAt));
        }

        return $deployEvent;
    }

    /**
     * @param string $dateTimeString
     * @return ?DateTimeImmutable
     */
    protected function parseDateTime(string $dateTimeString): ?DateTimeImmutable
    {
        return DateTimeImmutable::createFromFormat(
            DateTimeInterface::ISO8601_EXPANDED,
            $dateTimeString
        ) ?: null;
    }

    /**
     * @param SymfonyStyle $inputOutput
     * @param DeployEvent $deployEvent
     * @return void
     */
    protected function renderDeployEvent(SymfonyStyle $inputOutput, DeployEvent $deployEvent): void
    {
        $this->renderKeyValueTable(
            $inputOutput,
            [
                'Name' => $deployEvent->getName(),
                'Environment' => $deployEvent->getEnvironment(),
                'Version' => $deployEvent->getVersion(),
                'Built at' => $deployEvent
                    ->getBuiltAt()
                    ?->format(DateTimeInterface::ISO8601_EXPANDED),
                'Deployed at' => $deployEvent
                    ->getDeployedAt()
                    ?->format(DateTimeInterface::ISO8601_EXPANDED),
                'Git Branch' => $deployEvent->getGitCommitBranch(),
                'Git Commit Hash' => $deployEvent->getGitCommitHash(),
                'Git Commit Message' => $deployEvent->getGitCommitMessage()
            ]
        );
    }

    /**
     * @param SymfonyStyle $inputOutput
     * @param array<string, mixed> $table
     * @return void
     */
    protected function renderKeyValueTable(SymfonyStyle $inputOutput, array $table): void
    {
        $inputOutput->table(array_keys($table), [array_values($table)]);
    }
}
