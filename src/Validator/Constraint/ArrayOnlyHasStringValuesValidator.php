<?php

declare(strict_types=1);

namespace App\Validator\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * @see https://symfony.com/doc/current/validation.html
 * @see https://symfony.com/doc/current/reference/constraints.html
 */
class ArrayOnlyHasStringValuesValidator extends ConstraintValidator
{
    /**
     * {@inheritDoc}
     */
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof ArrayOnlyHasStringValues) {
            throw new UnexpectedTypeException($constraint, ArrayOnlyHasStringValues::class);
        }

        if (!is_array($value)) {
            return;
        }

        foreach ($value as $arrayValue) {
            if (!is_string($arrayValue)) {
                $this
                    ->context
                    ->buildViolation($constraint->message)
                    ->setCode(ArrayOnlyHasStringValues::ARRAY_NOT_ONLY_STRING_VALUES)
                    ->setInvalidValue($arrayValue)
                    ->addViolation();
            }
        }
    }
}
