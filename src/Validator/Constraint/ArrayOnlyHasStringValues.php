<?php

declare(strict_types=1);

namespace App\Validator\Constraint;

use Attribute;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @see https://symfony.com/doc/current/validation.html
 * @see https://symfony.com/doc/current/reference/constraints.html
 */
#[Attribute(Attribute::TARGET_PROPERTY | Attribute::TARGET_METHOD | Attribute::IS_REPEATABLE)]
class ArrayOnlyHasStringValues extends Constraint
{
    public const ARRAY_NOT_ONLY_STRING_VALUES = '8a87f168-ced3-44cb-81a3-bb6a0785d1b8';

    protected const ERROR_NAMES = [
        self::ARRAY_NOT_ONLY_STRING_VALUES => 'ARRAY_NOT_ONLY_STRING_VALUES',
    ];

    public string $message = "Array is only allowed to have string values.";

    /**
     * {@inheritDoc}
     */
    public function __construct(
        mixed $options = null,
        ?string $message = null,
        array $groups = null,
        mixed $payload = null
    ) {
        $this->message = $message ?? $this->message;

        parent::__construct($options, $groups, $payload);
    }

    /**
     * {@inheritDoc}
     *
     * @return class-string<ConstraintValidator>
     */
    public function validatedBy(): string
    {
        return ArrayOnlyHasStringValuesValidator::class;
    }
}
