<?php

declare(strict_types=1);

namespace App\Validator\Constraint;

use DateTime;
use DateTimeInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Validator to check if value is ISO8601
 */
class IsISO8601Validator extends ConstraintValidator
{
    /**
     * {@inheritDoc}
     */
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof IsISO8601) {
            throw new UnexpectedTypeException($constraint, IsISO8601::class);
        }

        if (null === $value) {
            return;
        }

        $violation = $this
            ->context
            ->buildViolation($constraint->message)
            ->setCode(IsISO8601::IS_NOT_ISO8601_ERROR)
            ->setInvalidValue($value);

        if (is_string($value)) {
            $dateTime = DateTime::createFromFormat(DateTimeInterface::ISO8601_EXPANDED, $value);

            if ($dateTime) {
                return;
            }

            $violation->setParameter('{{ string }}', $value);
        }

        $violation->addViolation();
    }
}
