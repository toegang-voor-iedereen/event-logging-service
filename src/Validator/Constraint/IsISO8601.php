<?php

declare(strict_types=1);

namespace App\Validator\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Constraint for validating if value is in ISO 8601 format.
 *
 * @see https://en.wikipedia.org/wiki/ISO_8601
 * @see https://symfony.com/doc/current/validation.html
 * @see https://symfony.com/doc/current/reference/constraints.html
 */
class IsISO8601 extends Constraint
{
    public const IS_NOT_ISO8601_ERROR = 'af5f292f-718e-4052-9a37-5cdae27cebde';

    protected const ERROR_NAMES = [
        self::IS_NOT_ISO8601_ERROR => 'IS_NOT_ISO8601_ERROR',
    ];

    public string $message = "Value is not in ISO 8601 format.";

    /**
     * {@inheritDoc}
     */
    public function __construct(
        mixed $options = null,
        ?string $message = null,
        array $groups = null,
        mixed $payload = null
    ) {
        $this->message = $message ?? $this->message;

        parent::__construct($options, $groups, $payload);
    }

    /**
     * {@inheritDoc}
     *
     * @return class-string<ConstraintValidator>
     */
    public function validatedBy(): string
    {
        return IsISO8601Validator::class;
    }
}
