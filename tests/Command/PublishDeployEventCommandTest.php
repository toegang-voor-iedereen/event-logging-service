<?php

declare(strict_types=1);

namespace App\Tests\Command;

use App\Entity\DeployEvent;
use App\Tests\TestUtil\Base\AbstractTestCase;
use DateTimeImmutable;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class PublishDeployEventCommandTest extends AbstractTestCase
{
    public function testCommandWithCorrectParameters(): void
    {
        $commandTester = $this->createCommandTester();
        $commandTester->execute($this->getValidParameters());
        $commandTester->assertCommandIsSuccessful();

        $this->assertEquals(
            <<<'STDOUT'
---------- ------------- --------- ---------------------------- ---------------------------- ------------ ----------------- --------------------
Name       Environment   Version   Built at                     Deployed at                  Git Branch   Git Commit Hash   Git Commit Message
---------- ------------- --------- ---------------------------- ---------------------------- ------------ ----------------- --------------------
test-app   production    1.0.0     +2021-01-01T00:00:00+00:00   +2021-01-01T00:00:00+00:00   master       1234567890        Initial commit
---------- ------------- --------- ---------------------------- ---------------------------- ------------ ----------------- --------------------


[OK] Persisted.


STDOUT,
            $this->trimAllLines($commandTester->getDisplay())
        );

        /**
         * @var ManagerRegistry $doctrine
         */
        $doctrine = $this->getContainer()->get('doctrine');

        $entityManager = $doctrine->getManager();
        $repository = $entityManager->getRepository(DeployEvent::class);
        $this->assertEquals(1, $repository->count([]));

        $deployEvent = $repository->findOneBy([]);

        $this->assertNotNull($deployEvent);

        $this->assertEquals('test-app', $deployEvent->getName());
        $this->assertEquals('1.0.0', $deployEvent->getVersion());
        $this->assertEquals('master', $deployEvent->getGitCommitBranch());
        $this->assertEquals('1234567890', $deployEvent->getGitCommitHash());
        $this->assertEquals('Initial commit', $deployEvent->getGitCommitMessage());
        $this->assertEqualsWithDelta(
            new DateTimeImmutable("now"),
            $deployEvent->getEventSubmittedAt(),
            1
        );
        $this->assertEquals(
            new DateTimeImmutable('2021-01-01T00:00:00+00:00'),
            $deployEvent->getBuiltAt()
        );
        $this->assertEquals(
            new DateTimeImmutable('2021-01-01T00:00:00+00:00'),
            $deployEvent->getDeployedAt()
        );
    }

    protected function createCommandTester(): CommandTester
    {
        $kernel = $this->bootKernel();
        $application = new Application($kernel);
        $command = $application->find('app:publish-deploy-event');

        $this->assertEquals(
            "Create DeployEvent",
            $command->getDescription()
        );

        return new CommandTester($command);
    }

    /**
     * @return array<string, string>
     */
    private function getValidParameters(): array
    {
        return [
            '--app-name' => 'test-app',
            '--environment' => 'production',
            '--app-version' => '1.0.0',
            '--built-at' => '2021-01-01T00:00:00+00:00',
            '--deployed-at' => '2021-01-01T00:00:00+00:00',
            '--git-branch' => 'master',
            '--git-commit-hash' => '1234567890',
            '--git-commit-message' => 'Initial commit',
        ];
    }

    /**
     * @dataProvider provideParameterKeys
     * @param string $parameterKey
     * @return void
     * @throws \Exception
     */
    public function testCommandWithMissingParameter(string $parameterKey): void
    {
        $commandTester = $this->createCommandTester();

        $parameters = $this->getValidParameters();

        // Missing or empty should technically yield the same effect.

        if (rand(0, 1) === 0) {
            $parameters[$parameterKey] = match (rand(1, 3)) {
                1 => '',
                2 => null,
                3 => ' ',
            };


        } else {
            unset($parameters[$parameterKey]);
        }

        $commandTester->execute($parameters);

        $this->assertEquals(
            1,
            $commandTester->getStatusCode()
        );

        $this->assertEquals(
            sprintf(
                "[ERROR] Error on option [%s]: This value should not be blank.",
                substr($parameterKey, 2)
            ),
            trim($commandTester->getDisplay())
        );

        /**
         * @var ManagerRegistry $doctrine
         */
        $doctrine = $this->getContainer()->get('doctrine');

        $entityManager = $doctrine->getManager();
        $repository = $entityManager->getRepository(DeployEvent::class);
        $this->assertEquals(0, $repository->count([]));
    }

    /**
     * @return string[][]
     */
    public function provideParameterKeys(): array
    {
        return array_map(
            fn (string $key): array => [$key],
            array_keys($this->getValidParameters())
        );
    }
}
