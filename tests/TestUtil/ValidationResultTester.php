<?php

declare(strict_types=1);

namespace App\Tests\TestUtil;

use Exception;
use PHPUnit\Framework\Assert;
use Symfony\Component\Validator\ConstraintViolationListInterface;

readonly class ValidationResultTester
{
    public function __construct(
        protected Assert $test,
        protected ConstraintViolationListInterface $violations,
        protected mixed $validatedValue
    ) {
    }

    /**
     * @throws Exception
     */
    public function assertNoViolations(): self
    {
        $this->test->assertEmpty($this->violations);
        $this->test->assertEquals(0, $this->violations->count());

        return $this;
    }

    public function assertViolationCount(int $count): self
    {
        $this->test->assertEquals($count, $this->violations->count());

        return $this;
    }

    /**
     * @param string $propertyPath
     * @param string $message
     * @return $this
     */
    public function assertHasViolation(string $propertyPath, string $message): self
    {
        foreach ($this->violations as $violation) {
            if (
                $violation->getPropertyPath() === $propertyPath &&
                $violation->getMessage() === $message
            ) {
                return $this;
            }
        }

        $this->test->fail(
            "No violation found with code: property path: `$propertyPath` and message: `$message`."
        );
    }
}
