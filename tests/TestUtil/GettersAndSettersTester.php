<?php

declare(strict_types=1);

namespace App\Tests\TestUtil;

use PHPUnit\Framework\Assert;

readonly class GettersAndSettersTester
{
    /**
     * @param Assert $test
     * @param object $instance
     * @param TestSubject $target
     */
    public function __construct(
        protected Assert $test,
        protected object $instance,
        protected TestSubject $target = TestSubject::GETTERS_AND_SETTERS
    ) {
    }

    /**
     * @param string $propertyKey
     * @param string|null $getter
     * @param string|null $setter
     * @return GettersAndSettersForPropertyTester
     */
    public function forProperty(
        string $propertyKey,
        ?string $getter = null,
        ?string $setter = null
    ): GettersAndSettersForPropertyTester {
        return new GettersAndSettersForPropertyTester(
            $this->test,
            $this->instance,
            $propertyKey,
            $getter,
            $setter,
            $this->target
        );
    }

    /**
     * @return GettersAndSettersForPropertiesTester
     */
    public function forProperties(): GettersAndSettersForPropertiesTester
    {
        return new GettersAndSettersForPropertiesTester(
            $this->test,
            $this->instance,
            $this->target
        );
    }
}
