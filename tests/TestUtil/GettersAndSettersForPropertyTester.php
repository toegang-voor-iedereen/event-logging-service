<?php

declare(strict_types=1);

namespace App\Tests\TestUtil;

use PHPUnit\Framework\Assert;

readonly class GettersAndSettersForPropertyTester
{
    /**
     * @param Assert $test
     * @param object $instance
     * @param string $propertyKey
     * @param string|null $getter
     * @param string|null $setter
     * @param TestSubject $target
     */
    public function __construct(
        protected Assert $test,
        protected object $instance,
        protected string $propertyKey,
        protected ?string $getter = null,
        protected ?string $setter = null,
        protected TestSubject $target = TestSubject::GETTERS_AND_SETTERS
    ) {
    }

    /**
     * @param mixed $value
     * @return void
     */
    public function work(mixed $value): void
    {
        $setter = null;

        if ($this->target->value & TestSubject::SETTERS->value) {
            $setter = $this->getSetter();

            $this->test->assertSame(
                $this->instance,
                $this->instance->$setter($value),
                sprintf(
                    "Setter `%s` on %s does not return type `self`.",
                    $setter,
                    get_class($this->instance)
                )
            );
        }

        if ($this->target->value & TestSubject::GETTERS->value) {
            $getter = $this->getGetter();

            $this->test->assertEquals(
                $this->instance->$getter(),
                $value,
                sprintf(
                    "Getter `%s` on %s does not return the value %s",
                    $getter,
                    get_class($this->instance),
                    $setter ? "that was set by setter `$setter`." : "that was expected."
                )
            );
        }
    }

    /**
     * @return string
     */
    protected function getSetter(): string
    {
        $possibleSetters = ['set' . ucfirst($this->propertyKey)];

        if ($this->setter) {
            $possibleSetters = [ $this->setter ];
        }

        $setter = $this->getMethodIfExists($possibleSetters);

        $this->test->assertIsString(
            $setter,
            "Setter not found for property $this->propertyKey. Tried: " . join(', ', $possibleSetters)
        );

        return $setter;
    }

    /**
     * @param string[] $methods
     * @return ?string
     */
    protected function getMethodIfExists(
        array $methods
    ): ?string {
        foreach ($methods as $method) {
            if (method_exists($this->instance, $method)) {
                return $method;
            }
        }

        return null;
    }

    /**
     * @return string
     */
    protected function getGetter(): string
    {
        $possibleGetters = [
            'get' . ucfirst($this->propertyKey),
            'set' . ucfirst($this->propertyKey)
        ];

        if ($this->getter) {
            $possibleGetters = [ $this->getter ];
        }

        $getter = $this->getMethodIfExists($possibleGetters);

        $this->test->assertIsString(
            $getter,
            "Setter not found for property $this->propertyKey. Tried: " . join(', ', $possibleGetters)
        );

        return $getter;
    }
}
