<?php

namespace App\Tests\TestUtil\Base;

use App\Tests\TestUtil\GettersAndSettersTester;
use App\Tests\TestUtil\TestSubject;
use App\Tests\TestUtil\ValidationResultTester;
use Exception;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractDtoTestCase extends AbstractTestCase
{
    protected ?ValidatorInterface $validator = null;

    /**
     * @throws Exception
     */
    protected function whenValidating(mixed $value): ValidationResultTester
    {
        return new ValidationResultTester(
            $this,
            $this->getValidator()->validate($value),
            $value
        );
    }

    /**
     * @throws Exception
     */
    protected function getValidator(): ValidatorInterface
    {
        $validator = $this->validator ?? $this->getContainer()->get(ValidatorInterface::class);

        if (!($validator instanceof ValidatorInterface)) {
            throw new Exception(
                sprintf(
                    "Expected validator to be an instance of %s, but got %s.",
                    ValidatorInterface::class,
                    $validator ? get_class($validator) : 'NULL'
                )
            );
        }

        $this->validator = $validator;

        return $this->validator;
    }

    protected function assertGetters(object $instance): GettersAndSettersTester
    {
        return new GettersAndSettersTester($this, $instance, TestSubject::GETTERS);
    }

    protected function assertGettersAndSetters(object $instance): GettersAndSettersTester
    {
        return new GettersAndSettersTester($this, $instance, TestSubject::GETTERS_AND_SETTERS);
    }
}
