<?php

namespace App\Tests\TestUtil\Base;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

abstract class AbstractTestCase extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    /**
     * @param string $string
     * @return string
     */
    protected function trimAllLines(string $string): string
    {
        return implode(
            "\n",
            array_map(
                'trim',
                explode("\n", $string)
            )
        );
    }
}
