<?php

declare(strict_types=1);

namespace App\Tests\TestUtil;

use PHPUnit\Framework\Assert;

readonly class GettersAndSettersForPropertiesTester
{
    /**
     * @param Assert $test
     * @param object $instance
     */
    public function __construct(
        protected Assert $test,
        protected object $instance,
        protected TestSubject $target = TestSubject::GETTERS_AND_SETTERS
    ) {
    }

    /**
     * @param array<string, mixed> $values
     * @return void
     */
    public function work(array $values): void
    {
        foreach ($values as $propertyKey => $value) {
            $forPropertyTester = new GettersAndSettersForPropertyTester(
                test: $this->test,
                instance: $this->instance,
                propertyKey: $propertyKey,
                target: $this->target
            );

            $forPropertyTester->work($value);
        }
    }
}
