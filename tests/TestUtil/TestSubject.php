<?php

declare(strict_types=1);

namespace App\Tests\TestUtil;

enum TestSubject: int
{
    case GETTERS = 1;
    case SETTERS = 2;
    case GETTERS_AND_SETTERS = 3;
}
