<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\DeployEvent;
use App\Factory\DeployEventFactory;
use App\Tests\TestUtil\Base\AbstractDtoTestCase;
use Exception;

class DeployEventTest extends AbstractDtoTestCase
{
    /**
     * @throws Exception
     */
    public function testGettersAndSetters(): void
    {
        $this->assertGettersAndSetters(
            new DeployEvent(),
        )->forProperties()->work(
            (new DeployEventFactory())
                ->getDefaults()
        );
    }
}
