<?php

namespace App\Tests\Endpoint;

use App\Entity\DeployEvent;
use App\Factory\DeployEventFactory;
use App\Tests\TestUtil\Base\AbstractApiTestCase;
use DateTime;
use DateTimeImmutable;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class DeployEventEndpointTest extends AbstractApiTestCase
{
    /**
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetEntity(): void
    {
        $deployEvent = DeployEventFactory::createOne();

        $response = $this
            ->createClient()
            ->request('GET', "/api/deploy_events/{$deployEvent->getUuid()}");
        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame(
            'content-type',
            'application/ld+json; charset=utf-8'
        );

        $this->assertEquals(
            [
                "@context" => "/api/contexts/DeployEvent",
                "@id" => "/api/deploy_events/{$deployEvent->getUuid()}",
                "@type" => "DeployEvent",
                "uuid" => $deployEvent->getUuid(),
                "name" => $deployEvent->getName(),
                "environment" => $deployEvent->getEnvironment(),
                "gitCommitHash" => $deployEvent->getGitCommitHash(),
                "gitCommitMessage" => $deployEvent->getGitCommitMessage(),
                "builtAt" => $deployEvent->getBuiltAt()?->format(DATE_ATOM),
                "deployedAt" => $deployEvent->getDeployedAt()?->format(DATE_ATOM),
                "eventSubmittedAt" => $deployEvent->getEventSubmittedAt()->format(DATE_ATOM),
                "gitCommitBranch" => $deployEvent->getGitCommitBranch(),
                "version" => $deployEvent->getVersion(),
            ],
            json_decode($response->getContent(), associative: true)
        );
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function testNotFoundEntity(): void
    {
        $this->createClient()->request(
            'GET',
            "/api/deploy_events/123456"
        );

        $this->assertResponseStatusCodeSame(404);
        $this->assertResponseHeaderSame(
            'content-type',
            'application/ld+json; charset=utf-8'
        );
    }

    /**
     * @return void
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testFilterLastEntities(): void
    {
        DeployEventFactory::createOne([
            "name" => 'TestLast',
            "environment" => 'env1',
            "version" => '1.0.3',
            "eventSubmittedAt" => DateTimeImmutable::createFromMutable((new DateTime('2022-01-01 13:45:10'))),
        ]);

        DeployEventFactory::createOne([
            "name" => 'TestLast',
            "environment" => 'env1',
            "version" => '1.0.5',
            "eventSubmittedAt" => DateTimeImmutable::createFromMutable((new DateTime('2022-01-01 13:47:10'))),
        ]);

        $last = DeployEventFactory::createOne([
            "name" => 'TestLast',
            "environment" => 'env1',
            "version" => '1.0.4',
            "eventSubmittedAt" => DateTimeImmutable::createFromMutable((new DateTime('2022-01-01 13:47:20'))),
        ]);

        $response = $this
            ->createClient()
            ->request(
                'GET',
                "/api/deploy_events?page=1&itemsPerPage=10&pagination=true&lasts=true"
            );

        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame(
            'content-type',
            'application/ld+json; charset=utf-8'
        );

        $responseContent = json_decode($response->getContent(), true);

        $this->assertIsArray($responseContent);
        $this->assertCount(1, $responseContent["hydra:member"]);

        $this->assertEquals(
            [
                "@id" => "/api/deploy_events/{$last->getUuid()}",
                "@type" => "DeployEvent",
                "uuid" => $last->getUuid(),
                "name" => $last->getName(),
                "environment" => $last->getEnvironment(),
                "gitCommitHash" => $last->getGitCommitHash(),
                "gitCommitMessage" => $last->getGitCommitMessage(),
                "builtAt" => $last->getBuiltAt()?->format(DATE_ATOM),
                "deployedAt" => $last->getDeployedAt()?->format(DATE_ATOM),
                "eventSubmittedAt" => $last->getEventSubmittedAt()->format(DATE_ATOM),
                "gitCommitBranch" => $last->getGitCommitBranch(),
                "version" => $last->getVersion(),
            ],
            $responseContent["hydra:member"][0] ?? null
        );
    }

    /**
     * @return void
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testFilterLastEntitiesWithNoEntities(): void
    {
        $response = $this
            ->createClient()
            ->request(
                'GET',
                "/api/deploy_events?pagination=false&lasts=true"
            );

        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame(
            'content-type',
            'application/ld+json; charset=utf-8'
        );

        $this->assertEquals(
            [
                "@context" => "/api/contexts/DeployEvent",
                "@id" => "/api/deploy_events",
                "@type" => "hydra:Collection",
                "hydra:member" => [],
                "hydra:totalItems" => 0,
                'hydra:view' => [
                    '@id' => '/api/deploy_events?pagination=false&lasts=true',
                    '@type' => 'hydra:PartialCollectionView',
                ],
                'hydra:search' => [
                    '@type' => 'hydra:IriTemplate',
                    'hydra:template' => '/api/deploy_events{?environment,environment[],lasts}',
                    'hydra:variableRepresentation' => 'BasicRepresentation',
                    'hydra:mapping' => [
                        [
                            '@type' => 'IriTemplateMapping',
                            'variable' => 'environment',
                            'property' => 'environment',
                            'required' => false
                        ],
                        [
                            '@type' => 'IriTemplateMapping',
                            'variable' => 'environment[]',
                            'property' => 'environment',
                            'required' => false
                        ],
                        [
                            '@type' => 'IriTemplateMapping',
                            'variable' => 'lasts',
                            'property' => 'lasts',
                            'required' => false
                        ],
                    ]
                ]
            ],
            json_decode($response->getContent(), true)
        );
    }

    /**
     * @dataProvider filterOnEnvironments
     *
     * @param string $environment
     * @param string $search
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testFilterEnvironment(
        string $environment,
        string $search
    ): void {
        $env = DeployEventFactory::createOne([
            "environment" => $environment,
        ]);

        $response = $this
            ->createClient()
            ->request(
                'GET',
                sprintf(
                    "/api/deploy_events?page=1&itemsPerPage=10&pagination=true&environment=%s",
                    $search
                )
            );

        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame(
            'content-type',
            'application/ld+json; charset=utf-8'
        );

        $responseContent = json_decode($response->getContent(), true);

        $this->assertIsArray($responseContent);

        $this->assertEquals(
            [
                "@id" => "/api/deploy_events/{$env->getUuid()}",
                "@type" => "DeployEvent",
                "uuid" => $env->getUuid(),
                "name" => $env->getName(),
                "environment" => $env->getEnvironment(),
                "gitCommitHash" => $env->getGitCommitHash(),
                "gitCommitMessage" => $env->getGitCommitMessage(),
                "builtAt" => $env->getBuiltAt()?->format(DATE_ATOM),
                "deployedAt" => $env->getDeployedAt()?->format(DATE_ATOM),
                "eventSubmittedAt" => $env->getEventSubmittedAt()->format(DATE_ATOM),
                "gitCommitBranch" => $env->getGitCommitBranch(),
                "version" => $env->getVersion(),
            ],
            $responseContent["hydra:member"][0] ?? null
        );
    }

    /**
     * @return string[][]
     */
    public function filterOnEnvironments(): array
    {
        return [
            ['env15', 'env15'],
            ['env67', 'env67'],
            ['env89', 'env89'],
        ];
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testFilterNotFoundOnEnvironment(): void
    {
        $this->createClient()->request(
            'GET',
            "/api/deploy_events?page=1&itemsPerPage=10&pagination=true&environment=NoEnvironment"
        );

        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame(
            'content-type',
            'application/ld+json; charset=utf-8'
        );

        $this->assertJsonContains([
            "@context" => "/api/contexts/DeployEvent",
            "hydra:totalItems" => 0
        ]);
    }

    /**
     * @return void
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testCreatePost(): void
    {
        $response = $this
            ->createClient()
            ->request('POST', "/api/deploy_events", [
                'json' => [
                    "name" => "element-structure-converter",
                    "environment" => "sandbox",
                    "version" => "1.2.3",
                    "gitCommitBranch" => "master",
                    "gitCommitHash" => "90c83d8d0efc9e783def5cfb3c036ac2fa31ce22",
                    "gitCommitMessage" => "NLDOC-XXX add git commit message to build info",
                    "builtAt" => "2023-09-04T18:46:40.030Z",
                    "deployedAt" => "2023-09-04T18:46:40.030Z",
                ],
            ]);

        $this->assertResponseStatusCodeSame(201);

        $this->assertResponseHeaderSame(
            'content-type',
            'application/ld+json; charset=utf-8'
        );

        $result = json_decode($response->getContent(), true);
        $this->assertIsArray($result);

        $this->assertMatchesResourceItemJsonSchema(DeployEvent::class);

        $this->assertArrayHasKey('@context', $result);
        $this->assertArrayHasKey('@id', $result);
        $this->assertArrayHasKey('@type', $result);
        $this->assertArrayHasKey('uuid', $result);
        $this->assertArrayHasKey('eventSubmittedAt', $result);

        $this->assertJsonContains(
            [
                "name" => "element-structure-converter",
                "environment" => "sandbox",
                "version" => "1.2.3",
                "gitCommitBranch" => "master",
                "gitCommitHash" => "90c83d8d0efc9e783def5cfb3c036ac2fa31ce22",
                "gitCommitMessage" => "NLDOC-XXX add git commit message to build info",
                "builtAt" => "2023-09-04T18:46:40+00:00",
                "deployedAt" => "2023-09-04T18:46:40+00:00",
            ]
        );
    }
}
