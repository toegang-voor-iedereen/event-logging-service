<?php

declare(strict_types=1);

namespace App\Tests\Filter;

use App\Filter\LastFilter;
use App\Tests\TestUtil\Base\AbstractDtoTestCase;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class LastFilterTest extends AbstractDtoTestCase
{
    /**
     * @throws ExceptionInterface
     */
    public function testConstructingWithEmptyProperties(): void
    {
        $this->expectExceptionObject(
            new InvalidArgumentException(
                "Expected some properties passed to App\Filter\LastFilter, but got none."
            )
        );

        new LastFilter(
            $this->createMock(NormalizerInterface::class),
            $this->createMock(ValidatorInterface::class),
            $this->createMock(EntityManagerInterface::class),
            []
        );
    }

    /**
     * @throws ExceptionInterface
     */
    public function testConstructingWithInvalidProperties(): void
    {
        $this->expectExceptionObject(
            new InvalidArgumentException(
                "orderByField: This value should not be blank."
            )
        );

        new LastFilter(
            $this->createMock(NormalizerInterface::class),
            $this->getValidator(),
            $this->createMock(EntityManagerInterface::class),
            [
                'orderByField' => '',
                'orderByDirection' => 'ASC',
                'groupBy' => ['foo', 'bar'],
            ]
        );
    }
}
