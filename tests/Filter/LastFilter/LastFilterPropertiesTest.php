<?php

declare(strict_types=1);

namespace App\Tests\Filter\LastFilter;

use App\Filter\LastFilter\LastFilterProperties;
use App\Tests\TestUtil\Base\AbstractDtoTestCase;
use Exception;

class LastFilterPropertiesTest extends AbstractDtoTestCase
{
    public function testConstructorAndGetters(): void
    {
        $this->assertGettersAndSetters(
            new LastFilterProperties()
        )
        ->forProperties()
        ->work([
            'groupBy' => ['foo', 'bar'],
            'orderByField' => 'baz',
            'orderByDirection' => 'ASC',
        ]);
    }

    /**
     * @throws Exception
     */
    public function testConstraintOnOrderBy(): void
    {
        $properties = new LastFilterProperties();
        $properties
            ->setOrderByDirection('as')
            ->setGroupBy([ 'foo', 'bar' ])
            ->setOrderByField('baz');

        $this->whenValidating($properties)
            ->assertViolationCount(1)
            ->assertHasViolation(
                'orderByDirection',
                'The value you selected is not a valid choice.'
            );
    }

    /**
     * @throws Exception
     */
    public function testConstraintOnGroupByOneOrMore(): void
    {
        $properties = new LastFilterProperties();
        $properties
            ->setOrderByDirection('ASC')
            ->setGroupBy([])
            ->setOrderByField('baz');

        $this->whenValidating($properties)
            ->assertViolationCount(1)
            ->assertHasViolation(
                'groupBy',
                'This collection should contain 1 element or more.'
            );
    }

    /**
     * @throws Exception
     */
    public function testConstraintOnGroupByOnlyStringValues(): void
    {
        $properties = new LastFilterProperties();
        $properties
            ->setOrderByDirection('ASC')
            ->setGroupBy([ 123 ])
            ->setOrderByField('baz');

        $this->whenValidating($properties)
            ->assertViolationCount(1)
            ->assertHasViolation(
                'groupBy',
                'Array is only allowed to have string values.'
            );
    }
    /**
     * @throws Exception
     */
    public function testConstraintOnOrderByFieldNotBlank(): void
    {
        $properties = new LastFilterProperties();
        $properties
            ->setOrderByDirection('ASC')
            ->setGroupBy([ ":D" ])
            ->setOrderByField('');

        $this->whenValidating($properties)
            ->assertViolationCount(1)
            ->assertHasViolation(
                'orderByField',
                'This value should not be blank.'
            );
    }
}
