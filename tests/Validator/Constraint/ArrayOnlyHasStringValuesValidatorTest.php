<?php

declare(strict_types=1);

namespace App\Tests\Validator\Constraint;

use App\Validator\Constraint\ArrayOnlyHasStringValues;
use App\Validator\Constraint\ArrayOnlyHasStringValuesValidator;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Validation;

class ArrayOnlyHasStringValuesValidatorTest extends KernelTestCase
{
    public function testValidatorGetsWrongConstraint(): void
    {
        $wrongConstraint = new NotBlank();

        $this->expectExceptionObject(
            new UnexpectedTypeException(
                $wrongConstraint,
                ArrayOnlyHasStringValues::class
            )
        );

        $constraintValidator = new ArrayOnlyHasStringValuesValidator();
        $constraintValidator->validate(
            'Some value but wrong constraint type',
            $wrongConstraint
        );
    }

    public function testArrayOfStrings(): void
    {
        $this->assertEquals(
            0,
            Validation::createValidator()
                ->validate(['foo', 'bar'], new ArrayOnlyHasStringValues())
                ->count()
        );
    }

    public function testEmptyArray(): void
    {
        $this->assertEquals(
            0,
            Validation::createValidator()
                ->validate([], new ArrayOnlyHasStringValues())
                ->count()
        );
    }

    public function testArrayWithInteger(): void
    {
        $validator = Validation::createValidator();

        $violations = $validator->validate([ 'string', 123 ], new ArrayOnlyHasStringValues());

        $this->assertEquals(1, $violations->count());

        $this->assertEquals(
            'Array is only allowed to have string values.',
            $violations->get(0)->getMessage()
        );

        $this->assertEquals(
            123,
            $violations->get(0)->getInvalidValue()
        );
    }

    public function testWithNonArray(): void
    {
        $this->assertEquals(
            0,
            Validation::createValidator()
                ->validate('Not an array', new ArrayOnlyHasStringValues())
                ->count()
        );
    }
}
