<?php

declare(strict_types=1);

namespace App\Tests\Validator\Constraint;

use App\Validator\Constraint\IsISO8601;
use App\Validator\Constraint\IsISO8601Validator;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Validation;

class IsISO8601ValidatorTest extends KernelTestCase
{
    public function testValidatorGetsWrongConstraint(): void
    {
        $wrongConstraint = new NotBlank();

        $this->expectExceptionObject(
            new UnexpectedTypeException(
                $wrongConstraint,
                IsISO8601::class
            )
        );

        $constraintValidator = new IsISO8601Validator();
        $constraintValidator->validate(
            'Some value but wrong constraint type',
            $wrongConstraint
        );
    }

    /**
     * @dataProvider validInputs
     * @param mixed $input
     * @return void
     */
    public function testValidInputs(mixed $input): void
    {
        $this->assertEquals(
            0,
            Validation::createValidator()
                ->validate($input, new IsISO8601())
                ->count(),
            sprintf(
                "Failed asserting that input is valid ISO8601. Input:\n\n%s\n",
                print_r($input, true)
            )
        );
    }

    /**
     * @dataProvider invalidInputs
     * @param mixed $input
     * @return void
     */
    public function testInvalidInputs(mixed $input): void
    {
        $validator = Validation::createValidator();

        $violations = $validator->validate($input, new IsISO8601());

        $this->assertEquals(1, $violations->count());

        $this->assertEquals(
            1,
            $violations->count(),
            sprintf(
                "Failed asserting that input is invalid ISO8601. Input:\n\n%s\n",
                print_r($input, true)
            )
        );

        $this->assertEquals(
            'Value is not in ISO 8601 format.',
            $violations->get(0)->getMessage()
        );

        $this->assertEquals(
            $input,
            $violations->get(0)->getInvalidValue()
        );
    }

    /**
     * @return mixed[][]
     */
    public function validInputs(): array
    {
        return array_map(
            fn (mixed $value): array => [$value],
            [
                null,
                "2023-09-05T15:24:12Z",
                "2023-09-05T15:19:56+0000"
            ]
        );
    }
    /**
     * @return mixed[][]
     */
    public function invalidInputs(): array
    {
        return array_map(
            fn (mixed $value): array => [$value],
            [
                [ "An array with a string" ],
                [ null ], [true], [123],
                "This is just a string",
                "2020-02-08T09:30:26.123+07:00",
            ]
        );
    }
}
